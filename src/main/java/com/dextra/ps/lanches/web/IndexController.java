package com.dextra.ps.lanches.web;

import com.dextra.ps.lanches.domain.repository.IngredientsRepository;
import com.dextra.ps.lanches.domain.repository.RecipesRepository;
import com.dextra.ps.lanches.domain.service.MenuService;
import com.dextra.ps.lanches.web.dto.SnackDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
@RequestMapping("")
public class IndexController {

    private final IngredientsRepository ingredientsRepository;

    private final RecipesRepository recipesRepository;

    private final MenuService menuService;

    @Autowired
    public IndexController(final IngredientsRepository ingredientsRepository,
                           final RecipesRepository recipesRepository,
                           final MenuService menuService) {

        this.ingredientsRepository = ingredientsRepository;
        this.recipesRepository = recipesRepository;
        this.menuService = menuService;
    }

    @GetMapping("")
    public String index(final Model model) {

        model.addAttribute("ingredients", ingredientsRepository.findAll());
        model.addAttribute("recipes", recipesRepository.findAll());
        model.addAttribute("snacks", menuService.findAllSnacks().stream().map(SnackDTO::fromSnack).collect(Collectors.toList()));

        return "index";
    }
}
