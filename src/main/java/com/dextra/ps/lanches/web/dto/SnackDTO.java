package com.dextra.ps.lanches.web.dto;

import com.dextra.ps.lanches.domain.entity.Snack;

import java.util.stream.Collectors;

public class SnackDTO {

    private String name;

    private Integer price;

    private String imageUrl;

    private String ingredients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public static SnackDTO fromSnack(final Snack snack) {

        final SnackDTO snackDTO = new SnackDTO();
        snackDTO.setName(snack.getRecipe().getName());
        snackDTO.setPrice(snack.getPrice());
        snackDTO.setImageUrl(snack.getRecipe().getImageUrl());

        final String ingredients = snack.getRecipe()
                .getItems()
                .stream()
                .map(item -> item.getIngredient().getName() + " (x" + item.getAmount() + ")")
                .collect(Collectors.joining(", "));

        snackDTO.setIngredients(ingredients);

        return snackDTO;
    }
}
