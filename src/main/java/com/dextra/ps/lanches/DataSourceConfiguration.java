package com.dextra.ps.lanches;

import com.dextra.ps.lanches.domain.db.DataSource;
import com.dextra.ps.lanches.domain.db.InMemoryDataSource;
import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;
import com.dextra.ps.lanches.domain.entity.Recipe.RecipeItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class DataSourceConfiguration {

    @Value("${application.ingredient.lettuceId}")
    private Long lettuceId;

    @Value("${application.ingredient.baconId}")
    private Long baconId;

    @Value("${application.ingredient.beefBurgerId}")
    private Long beefBurgerId;

    @Value("${application.ingredient.eggId}")
    private Long eggId;

    @Value("${application.ingredient.cheeseId}")
    private Long cheeseId;

    @Bean
    DataSource dataSource() {

        final Ingredient lettuce = new Ingredient(lettuceId, "Alface", 40, "http://content.paodeacucar.com/wp-content/uploads/2018/02/tipos-de-alface-lisa.jpg");
        final Ingredient bacon = new Ingredient(baconId, "Bacon", 200, "http://raw.cdn.cennoticias.com/112fa77c-a7ac-439f-9f58-661597cf5870");
        final Ingredient beefBurger = new Ingredient(beefBurgerId, "Hambúrguer de carne", 300, "https://www.billsbarandburger.com/content/uploads/2015/07/Burger-Picture-700x400.jpg");
        final Ingredient egg = new Ingredient(eggId, "Ovo", 80, "https://cms.splendidtable.org/sites/default/files/styles/lede_image/public/ThinkstockPhotos-479103323.jpg");
        final Ingredient cheese = new Ingredient(cheeseId, "Queijo", 150, "https://content.paodeacucar.com/wp-content/uploads/2018/07/queijos-duros-cheddar.jpg");

        long snackId = 0;

        final Recipe xBacon = new Recipe(++snackId, "X-Bacon", Arrays.asList(
                new RecipeItem(bacon, 1),
                new RecipeItem(beefBurger, 1),
                new RecipeItem(cheese, 1)
        ), "http://raw.cdn.cennoticias.com/ae055b12-c576-44e5-a4bd-37dc077b2bfc");

        final Recipe xBurger = new Recipe(++snackId, "X-Burger", Arrays.asList(
                new RecipeItem(beefBurger, 1),
                new RecipeItem(cheese, 1)
        ), "https://www.portaldofranchising.com.br/wp-content/uploads/2017/09/franquias-de-sanduiches-franquia-burger.jpg");

        final Recipe xEgg = new Recipe(++snackId, "X-Egg", Arrays.asList(
                new RecipeItem(egg, 1),
                new RecipeItem(beefBurger, 1),
                new RecipeItem(cheese, 1)
        ), "https://i.pinimg.com/originals/a0/c4/14/a0c4143a545871f9495f1c9c0a8c9f96.jpg");

        final Recipe xEggBacon = new Recipe(++snackId, "X-EggBacon", Arrays.asList(
                new RecipeItem(egg, 1),
                new RecipeItem(bacon, 1),
                new RecipeItem(beefBurger, 1),
                new RecipeItem(cheese, 1)
        ), "https://arizonamedicalmarijuanaclinic.com/wp-content/uploads/2017/11/d3atagt0rnqk7k.cloudfront.nethigh-in-las-vegas-burger-e21049028cdf0d2a927156d19741c8ac4e1bd139-700x400.jpg");

        return new InMemoryDataSource(
                Arrays.asList(lettuce, bacon, beefBurger, egg, cheese),
                Arrays.asList(xBacon, xBurger, xEgg, xEggBacon)
        );
    }
}
