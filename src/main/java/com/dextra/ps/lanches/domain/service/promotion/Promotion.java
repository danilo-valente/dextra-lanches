package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.entity.Recipe;

public interface Promotion {

    /**
     * Determine the priority in which a Promotion should be applied to a given Recipe
     * @return the promotion's priority
     */
    int priority();

    /**
     * If this promotion matches the pre-requisites for a given Recipe apply the corresponding discount. Otherwise, just return the original price
     * @param recipe the recipe in which the discount should be applied
     * @param currentPrice initial price
     * @return the resulting price after applying the promotion's discount
     */
    int applyDiscount(Recipe recipe, int currentPrice);
}
