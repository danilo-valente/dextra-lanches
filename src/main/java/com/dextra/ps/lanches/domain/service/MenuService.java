package com.dextra.ps.lanches.domain.service;

import com.dextra.ps.lanches.domain.entity.Recipe;
import com.dextra.ps.lanches.domain.entity.Snack;
import com.dextra.ps.lanches.domain.repository.RecipesRepository;
import com.dextra.ps.lanches.domain.service.promotion.Promotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuService {

    private final RecipesRepository recipesRepository;

    private final List<Promotion> promotions;

    @Autowired
    public MenuService(final RecipesRepository recipesRepository,
                       final List<Promotion> promotions) {

        this.recipesRepository = recipesRepository;

        // Sort promotions according to their priority
        this.promotions = promotions.stream()
                .sorted((p1, p2) -> p2.priority() - p1.priority())
                .collect(Collectors.toList());
    }

    /**
     * Build a list of all available Snacks and their respective prices according to available promotions
     * @return the list of available Snacks
     */
    public List<Snack> findAllSnacks() {

        return recipesRepository.findAll().stream()
                .map(this::findSnackByRecipe)
                .collect(Collectors.toList());
    }

    /**
     * Build a Snack object from a Recipe and calculate its price according to available promotions
     * @param recipe the given Recipe
     * @return the Snack object
     */
    public Snack findSnackByRecipe(final Recipe recipe) {
        Assert.notNull(recipe, "Cannot find snack of null recipe");

        return new Snack(recipe, calculateSnackPriceFromRecipe(recipe));
    }

    /**
     * Calculate the price for a given Recipe according to available promotions
     * @param recipe the given Recipe
     * @return the snack price
     */
    public int calculateSnackPriceFromRecipe(final Recipe recipe) {
        Assert.notNull(recipe, "Cannot calculate price of null recipe");

        final int initialPrice = recipe.getItems().stream()
                .mapToInt(item -> item.getIngredient().getPrice() * item.getAmount())
                .sum();

        return promotions.stream()
                .reduce(initialPrice, (currentPrice, promotion) -> promotion.applyDiscount(recipe, currentPrice), Integer::sum);
    }
}
