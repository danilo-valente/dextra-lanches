package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.repository.IngredientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExtraCheesePromotion extends ExtraIngredientPromotion {

    @Autowired
    public ExtraCheesePromotion(final IngredientsRepository ingredientsRepository,
                                @Value("${application.ingredient.cheeseId}") final Long cheeseId,
                                @Value("${application.promotion.extraIngredient.discountAmount}") final Integer discountAmount,
                                @Value("${application.promotion.extraIngredient.totalAmount}") final Integer totalAmount) {

        super(ingredientsRepository, cheeseId, discountAmount, totalAmount);
    }
}
