package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;
import com.dextra.ps.lanches.domain.repository.IngredientsRepository;
import org.springframework.util.Assert;

public abstract class ExtraIngredientPromotion implements Promotion {

    private final IngredientsRepository ingredientsRepository;

    private final Long ingredientId;

    private final Integer discountAmount;

    private final Integer totalAmount;

    public ExtraIngredientPromotion(final IngredientsRepository ingredientsRepository,
                                    final Long ingredientId,
                                    final Integer discountAmount,
                                    final Integer totalAmount) {

        this.ingredientsRepository = ingredientsRepository;
        this.ingredientId = ingredientId;
        this.discountAmount = discountAmount;
        this.totalAmount = totalAmount;
    }

    /**
     * Determine the priority in which a Promotion should be applied to a given Recipe
     * @return the promotion's priority
     */
    @Override
    public int priority() {
        return 20;
    }

    /**
     * Gives for free {@code this.discountAmount} ingredients which id={@code this.ingredientId} for each {@code this.totalAmount} ingredients of the same id
     * @param recipe the recipe in which the discount should be applied
     * @param currentPrice initial price
     * @return the resulting price after applying the promotion's discount
     */
    @Override
    public int applyDiscount(final Recipe recipe, final int currentPrice) {
        Assert.notNull(recipe, "Cannot apply discount on a null recipe");
        Assert.notNull(recipe.getItems(), "Cannot apply discount on a recipe with null item list");
        Assert.isTrue(currentPrice >= 0, "Invalid currentPrice = " + currentPrice);

        final Ingredient ingredient = ingredientsRepository.findById(ingredientId);

        final int ingredientCount = recipe.getItems().stream()
                .filter(item -> ingredientId.equals(item.getIngredient().getId()))
                .map(Recipe.RecipeItem::getAmount)
                .reduce(0, Integer::sum);

        final int discount = ingredient.getPrice() * (ingredientCount * discountAmount / totalAmount);

        return currentPrice - discount;
    }
}
