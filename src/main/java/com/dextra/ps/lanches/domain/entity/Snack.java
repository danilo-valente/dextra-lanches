package com.dextra.ps.lanches.domain.entity;

import java.util.Objects;

public class Snack {

    private Recipe recipe;

    private Integer price;

    public Snack() {
    }

    public Snack(Recipe recipe, Integer price) {
        this.recipe = recipe;
        this.price = price;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snack snack = (Snack) o;
        return Objects.equals(recipe, snack.recipe) &&
                Objects.equals(price, snack.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipe, price);
    }

    @Override
    public String toString() {
        return "Snack{" +
                "recipe=" + recipe +
                ", price=" + price +
                '}';
    }
}
