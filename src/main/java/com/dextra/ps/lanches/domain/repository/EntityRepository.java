package com.dextra.ps.lanches.domain.repository;

import com.dextra.ps.lanches.domain.entity.Entity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntityRepository<T extends Entity> {

    T create(T entity);

    T update(T entity);

    T findById(Long id);

    List<T> findAll();

    T delete(Long id);
}
