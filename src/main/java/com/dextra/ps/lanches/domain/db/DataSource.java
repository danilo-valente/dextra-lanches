package com.dextra.ps.lanches.domain.db;

import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;

import java.util.List;

public interface DataSource {

    List<Ingredient> getIngredients();

    void setIngredients(List<Ingredient> ingredients);

    Long nextIngredientId();

    List<Recipe> getRecipes();

    void setRecipes(List<Recipe> recipes);

    Long nextSnackId();

    Long nextPromotionId();
}
