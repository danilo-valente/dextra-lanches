package com.dextra.ps.lanches.domain.db;

import com.dextra.ps.lanches.domain.entity.Entity;
import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class InMemoryDataSource implements DataSource {

    private List<Ingredient> ingredients;

    private List<Recipe> recipes;

    public InMemoryDataSource() {
        this.ingredients = new ArrayList<>();
        this.recipes = new ArrayList<>();
    }

    public InMemoryDataSource(final List<Ingredient> ingredients,
                              final List<Recipe> recipes) {
        this.ingredients = ingredients;
        this.recipes = recipes;
    }

    @Override
    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public Long nextIngredientId() {
        final Long maxId = ingredients.stream()
                .map(Entity::getId)
                .max(Comparator.naturalOrder())
                .orElse(0L);

        return maxId + 1;
    }

    @Override
    public List<Recipe> getRecipes() {
        return recipes;
    }

    @Override
    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public Long nextSnackId() {
        final Long maxId = recipes.stream()
                .map(Entity::getId)
                .max(Comparator.naturalOrder())
                .orElse(0L);

        return maxId + 1;
    }

    @Override
    public Long nextPromotionId() {
        final Long maxId = recipes.stream()
                .map(Entity::getId)
                .max(Comparator.naturalOrder())
                .orElse(0L);

        return maxId + 1;
    }
}
