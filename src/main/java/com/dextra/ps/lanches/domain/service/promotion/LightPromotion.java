package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.entity.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class LightPromotion implements Promotion {

    private final Long lettuceId;

    private final Long baconId;

    private final Float discountPercent;

    @Autowired
    public LightPromotion(@Value("${application.ingredient.lettuceId}") final Long lettuceId,
                          @Value("${application.ingredient.baconId}") final Long baconId,
                          @Value("${application.promotion.light.discount}") final Float discountPercent) {

        this.lettuceId = lettuceId;
        this.baconId = baconId;
        this.discountPercent = discountPercent;
    }

    /**
     * Determine the priority in which a Promotion should be applied to a given Recipe
     * @return the promotion's priority
     */
    @Override
    public int priority() {
        return 10;
    }

    /**
     * Apply a 10% discount whenever the given Recipe does not include any bacon but has at least one portion of lettuce
     * @param recipe the recipe in which the discount should be applied
     * @param currentPrice initial price
     * @return the resulting price after applying the promotion's discount
     */
    @Override
    public int applyDiscount(final Recipe recipe, final int currentPrice) {
        Assert.notNull(recipe, "Cannot apply discount on a null recipe");
        Assert.notNull(recipe.getItems(), "Cannot apply discount on a recipe with null item list");
        Assert.isTrue(currentPrice >= 0, "Invalid currentPrice = " + currentPrice);

        final boolean hasLettuce = recipe.getItems().stream()
                .anyMatch(item -> lettuceId.equals(item.getIngredient().getId()));

        final boolean hasBacon = recipe.getItems().stream()
                .anyMatch(item -> baconId.equals(item.getIngredient().getId()));

        return Math.round(hasLettuce && !hasBacon ? currentPrice * (1 - discountPercent) : currentPrice);
    }
}
