package com.dextra.ps.lanches.domain.repository;

import com.dextra.ps.lanches.domain.db.DataSource;
import com.dextra.ps.lanches.domain.entity.Ingredient;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

@Repository
public class IngredientsRepository implements EntityRepository<Ingredient> {

    private DataSource dataSource;

    public IngredientsRepository(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Ingredient create(final Ingredient ingredient) {

        final Ingredient createdIngredient = new Ingredient(dataSource.nextIngredientId(), ingredient.getName(), ingredient.getPrice(), ingredient.getImageUrl());

        dataSource.getIngredients().add(createdIngredient);

        return createdIngredient;
    }

    @Override
    public Ingredient update(final Ingredient ingredient) {

        Assert.notNull(ingredient, "Ingredient cannot be null");
        Assert.notNull(ingredient.getId(), "Ingredient ID cannot be null");

        final Ingredient currentIngredient = findById(ingredient.getId());
        currentIngredient.setName(ingredient.getName());
        currentIngredient.setPrice(ingredient.getPrice());

        return currentIngredient;
    }

    @Override
    public Ingredient findById(final Long id) {

        return dataSource.getIngredients().stream()
                .filter(ingredient -> ingredient.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can't find ingredient with id=" + id));
    }

    @Override
    public List<Ingredient> findAll() {
        return dataSource.getIngredients();
    }

    @Override
    public Ingredient delete(final Long id) {

        final Ingredient deletedIngredient = findById(id);

        dataSource.getIngredients().remove(deletedIngredient);

        return deletedIngredient;
    }
}
