package com.dextra.ps.lanches.domain.repository;

import com.dextra.ps.lanches.domain.db.DataSource;
import com.dextra.ps.lanches.domain.entity.Recipe;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;

@Repository
public class RecipesRepository implements EntityRepository<Recipe> {

    private DataSource dataSource;

    public RecipesRepository(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Recipe create(final Recipe recipe) {

        final Recipe createdRecipe = new Recipe(dataSource.nextSnackId(), recipe.getName(), recipe.getItems(), recipe.getImageUrl());

        dataSource.getRecipes().add(createdRecipe);

        return createdRecipe;
    }

    @Override
    public Recipe update(final Recipe recipe) {

        Assert.notNull(recipe, "Recipe cannot be null");
        Assert.notNull(recipe.getId(), "Recipe ID cannot be null");

        final Recipe currentRecipe = findById(recipe.getId());
        currentRecipe.setName(recipe.getName());
        currentRecipe.setItems(recipe.getItems());

        return currentRecipe;
    }

    @Override
    public Recipe findById(final Long id) {

        return dataSource.getRecipes().stream()
                .filter(recipe -> recipe.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Can't find recipe with id=" + id));
    }

    @Override
    public List<Recipe> findAll() {
        return dataSource.getRecipes();
    }

    @Override
    public Recipe delete(final Long id) {

        final Recipe deletedRecipe = findById(id);

        dataSource.getRecipes().remove(deletedRecipe);

        return deletedRecipe;
    }
}
