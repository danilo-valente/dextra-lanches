package com.dextra.ps.lanches.domain.entity;

import java.util.List;
import java.util.Objects;

public class Recipe extends Entity {

    private String name;

    private List<RecipeItem> items;

    private String imageUrl;

    public Recipe() {
    }

    public Recipe(Long id, String name, List<RecipeItem> items, String imageUrl) {
        super(id);

        this.name = name;
        this.items = items;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeItem> getItems() {
        return items;
    }

    public void setItems(List<RecipeItem> items) {
        this.items = items;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Recipe recipe = (Recipe) o;
        return Objects.equals(name, recipe.name) &&
                Objects.equals(items, recipe.items) &&
                Objects.equals(imageUrl, recipe.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, items, imageUrl);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", items=" + items +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    public static class RecipeItem {

        private Ingredient ingredient;

        private Integer amount;

        public RecipeItem() {
        }

        public RecipeItem(Ingredient ingredient, Integer amount) {
            this.ingredient = ingredient;
            this.amount = amount;
        }

        public Ingredient getIngredient() {
            return ingredient;
        }

        public void setIngredient(Ingredient ingredient) {
            this.ingredient = ingredient;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        @Override
        public String toString() {
            return "RecipeItem{" +
                    "ingredient=" + ingredient +
                    ", amount=" + amount +
                    '}';
        }
    }
}
