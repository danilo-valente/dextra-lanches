<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dextra Lanches</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/webjars/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Dextra Lanches</a>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">
            <h1 class="my-4">Menu</h1>

            <div class="list-group">
                <a href="/" class="list-group-item">Card&aacute;pio</a>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid"
                             src="https://cocobambu.com/wp-content/uploads/2018/07/cardapio.jpg" alt="First slide">
                    </div>
                </div>
            </div>

            <div class="row">
                <c:forEach var="snack" items="${snacks}">

                    <div class="col-lg-6 col-md-6 mb-4">
                        <div class="card h-100">
                            <img class="card-img-top" src="${snack.imageUrl}" alt="">
                            <div class="card-body">
                                <h4 class="card-title">${snack.name}</h4>
                                <h5><fmt:formatNumber value="${snack.price / 100}" type="currency"/></h5>
                                <p class="card-text">${snack.ingredients}</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Dextra Lanches 2019</p>
    </div>
    <!-- /.container -->
</footer>

<script src="/webjars/jquery/3.3.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>