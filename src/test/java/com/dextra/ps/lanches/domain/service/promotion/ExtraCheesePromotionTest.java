package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;

import static com.dextra.ps.lanches.utils.TestUtils.recipeWithItems;
import static com.dextra.ps.lanches.utils.TestUtils.recipeWithSingleItem;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PromotionTestsConfiguration.class, properties = {
        "application.ingredient.lettuceId=1",
        "application.ingredient.baconId=2",
        "application.ingredient.beefBurgerId=3",
        "application.ingredient.eggId=4",
        "application.ingredient.cheeseId=5",
        "application.promotion.light.discount=0.1",
        "application.promotion.extraIngredient.discountAmount=1",
        "application.promotion.extraIngredient.totalAmount=3"
})
public class ExtraCheesePromotionTest {

    @Autowired
    @Qualifier("lettuceIngredient")
    private Ingredient lettuce;

    @Autowired
    @Qualifier("baconIngredient")
    private Ingredient bacon;

    @Autowired
    @Qualifier("beefBurgerIngredient")
    private Ingredient beefBurger;

    @Autowired
    @Qualifier("cheeseIngredient")
    private Ingredient cheese;

    @Autowired
    public ExtraCheesePromotion extraCheesePromotion;

    @Test(expected = IllegalArgumentException.class)
    public void applyDiscount_NullRecipeGiven_ShouldThrowException() {

        extraCheesePromotion.applyDiscount(null, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void applyDiscount_RecipeWithNullItemListGiven_ShouldThrowException() {

        extraCheesePromotion.applyDiscount(new Recipe(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void applyDiscount_NegativeCurrentPriceGiven_ShouldThrowException() {

        final Recipe emptyRecipe = new Recipe(1L, "", Collections.emptyList(), "");

        extraCheesePromotion.applyDiscount(emptyRecipe, -1);
    }

    @Test
    public void applyDiscount_LettuceRecipeGiven_ShouldApplyDiscount() {

        final Recipe lettuceRecipe = recipeWithSingleItem(lettuce, 1);

        assertEquals(extraCheesePromotion.applyDiscount(lettuceRecipe, 1000), 1000);
    }

    @Test
    public void applyDiscount_BaconRecipeGiven_ShouldNotApplyDiscount() {

        final Recipe baconRecipe = recipeWithSingleItem(bacon, 1);

        assertEquals(extraCheesePromotion.applyDiscount(baconRecipe, 1000), 1000);
    }

    @Test
    public void applyDiscount_LettuceAndBaconRecipeGiven_ShouldNotApplyDiscount() {

        final Recipe lettuceAndBaconRecipe = recipeWithItems(Arrays.asList(
                new Recipe.RecipeItem(lettuce, 1),
                new Recipe.RecipeItem(bacon, 1)
        ));

        assertEquals(extraCheesePromotion.applyDiscount(lettuceAndBaconRecipe, 1200), 1200);
    }

    @Test
    public void applyDiscount_CheeseRecipeGiven_ShouldApplyDiscount() {

        final Recipe cheeseRecipe = recipeWithSingleItem(cheese, 1);

        assertEquals(extraCheesePromotion.applyDiscount(cheeseRecipe, 500), 500);
    }

    @Test
    public void applyDiscount_LettuceAndDoubleCheeseRecipeGiven_ShouldApplyDiscount() {

        final Recipe lettuceAndDoubleCheeseRecipe = recipeWithItems(Arrays.asList(
                new Recipe.RecipeItem(lettuce, 1),
                new Recipe.RecipeItem(cheese, 2)
        ));

        assertEquals(extraCheesePromotion.applyDiscount(lettuceAndDoubleCheeseRecipe, 1200), 1200);
    }

    @Test
    public void applyDiscount_TripleBeefBurgerLettuceRecipeGiven_ShouldApplyDiscount() {

        final Recipe lettuceAndTripleBeefBurgerRecipe = recipeWithItems(Arrays.asList(
                new Recipe.RecipeItem(lettuce, 1),
                new Recipe.RecipeItem(beefBurger, 3)
        ));

        assertEquals(extraCheesePromotion.applyDiscount(lettuceAndTripleBeefBurgerRecipe, 3200), 3200);
    }

    @Test
    public void applyDiscount_TripleCheeseLettuceRecipeGiven_ShouldApplyDiscount() {

        final Recipe lettuceAndTripleCheeseRecipe = recipeWithItems(Arrays.asList(
                new Recipe.RecipeItem(lettuce, 1),
                new Recipe.RecipeItem(cheese, 3)
        ));

        assertEquals(extraCheesePromotion.applyDiscount(lettuceAndTripleCheeseRecipe, 1700), 1200);
    }
}
