package com.dextra.ps.lanches.domain.service.promotion;

import com.dextra.ps.lanches.domain.db.DataSource;
import com.dextra.ps.lanches.domain.db.InMemoryDataSource;
import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;
import com.dextra.ps.lanches.domain.repository.IngredientsRepository;
import com.dextra.ps.lanches.domain.repository.RecipesRepository;
import com.dextra.ps.lanches.domain.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class PromotionTestsConfiguration {

    @Value("${application.ingredient.lettuceId}")
    private Long lettuceId;

    @Value("${application.ingredient.baconId}")
    private Long baconId;

    @Value("${application.ingredient.beefBurgerId}")
    private Long beefBurgerId;

    @Value("${application.ingredient.eggId}")
    private Long eggId;

    @Value("${application.ingredient.cheeseId}")
    private Long cheeseId;

    @Bean(name = "lettuceIngredient")
    Ingredient lettuceIngredient() {
        return new Ingredient(lettuceId, "Alface", 200, "");
    }

    @Bean(name = "baconIngredient")
    Ingredient baconIngredient() {
        return new Ingredient(baconId, "Bacon", 1000, "");
    }

    @Bean(name = "beefBurgerIngredient")
    Ingredient beefBurgerIngredient() {
        return new Ingredient(beefBurgerId, "Hambúrguer de carne", 1000, "");
    }

    @Bean(name = "eggIngredient")
    Ingredient eggIngredient() {
        return new Ingredient(eggId, "Ovo", 400, "");
    }

    @Bean(name = "cheeseIngredient")
    Ingredient cheeseIngredient() {
        return new Ingredient(cheeseId, "Queijo", 500, "");
    }

    @Bean(name = "xBaconRecipe")
    Recipe xBacon(@Qualifier("baconIngredient") final Ingredient bacon,
                  @Qualifier("beefBurgerIngredient") final Ingredient beefBurger,
                  @Qualifier("cheeseIngredient") final Ingredient cheese) {

        return new Recipe(1L, "X-Bacon", Arrays.asList(
                new Recipe.RecipeItem(bacon, 1),
                new Recipe.RecipeItem(beefBurger, 1),
                new Recipe.RecipeItem(cheese, 1)
        ), "");
    }

    @Bean(name = "xBurgerRecipe")
    Recipe xBurger(@Qualifier("beefBurgerIngredient") final Ingredient beefBurger,
                   @Qualifier("cheeseIngredient") final Ingredient cheese) {

        return new Recipe(2L, "X-Burger", Arrays.asList(
                new Recipe.RecipeItem(beefBurger, 1),
                new Recipe.RecipeItem(cheese, 1)
        ), "");
    }

    @Bean(name = "xEggRecipe")
    Recipe xEgg(@Qualifier("eggIngredient") final Ingredient egg,
                @Qualifier("beefBurgerIngredient") final Ingredient beefBurger,
                @Qualifier("cheeseIngredient") final Ingredient cheese) {

        return new Recipe(3L, "X-Egg", Arrays.asList(
                new Recipe.RecipeItem(egg, 1),
                new Recipe.RecipeItem(beefBurger, 1),
                new Recipe.RecipeItem(cheese, 1)
        ), "");
    }

    @Bean(name = "xEggBaconRecipe")
    Recipe xEggBacon(@Qualifier("baconIngredient") final Ingredient bacon,
                     @Qualifier("eggIngredient") final Ingredient egg,
                     @Qualifier("beefBurgerIngredient") final Ingredient beefBurger,
                     @Qualifier("cheeseIngredient") final Ingredient cheese) {

        return new Recipe(4L, "X-EggBacon", Arrays.asList(
                new Recipe.RecipeItem(bacon, 1),
                new Recipe.RecipeItem(egg, 1),
                new Recipe.RecipeItem(beefBurger, 1),
                new Recipe.RecipeItem(cheese, 1)
        ), "");
    }

    @Bean
    DataSource dataSource(@Qualifier("lettuceIngredient") Ingredient lettuce,
                          @Qualifier("baconIngredient") Ingredient bacon,
                          @Qualifier("beefBurgerIngredient") Ingredient beefBurger,
                          @Qualifier("eggIngredient") Ingredient egg,
                          @Qualifier("cheeseIngredient") Ingredient cheese) {

        return new InMemoryDataSource(
                Arrays.asList(lettuce, bacon, beefBurger, egg, cheese),
                Collections.emptyList()
        );
    }

    @Bean
    IngredientsRepository ingredientsRepository(final DataSource dataSource) {
        return new IngredientsRepository(dataSource);
    }

    @Bean
    RecipesRepository recipesRepository(final DataSource dataSource) {
        return new RecipesRepository(dataSource);
    }

    @Bean
    LightPromotion lightPromotion(@Value("${application.ingredient.lettuceId}") final Long lettuceId,
                                  @Value("${application.ingredient.baconId}") final Long baconId,
                                  @Value("${application.promotion.light.discount}") final Float discountPercent) {

        return new LightPromotion(lettuceId, baconId, discountPercent);
    }

    @Bean
    ExtraMeatPromotion extraMeatPromotion(final IngredientsRepository ingredientsRepository,
                                          @Value("${application.ingredient.beefBurgerId}") final Long beefBurgerId,
                                          @Value("${application.promotion.extraIngredient.discountAmount}") final Integer discountAmount,
                                          @Value("${application.promotion.extraIngredient.totalAmount}") final Integer totalAmount) {

        return new ExtraMeatPromotion(ingredientsRepository, beefBurgerId, discountAmount, totalAmount);
    }

    @Bean
    ExtraCheesePromotion extraCheesePromotion(final IngredientsRepository ingredientsRepository,
                                              @Value("${application.ingredient.cheeseId}") final Long cheeseid,
                                              @Value("${application.promotion.extraIngredient.discountAmount}") final Integer discountAmount,
                                              @Value("${application.promotion.extraIngredient.totalAmount}") final Integer totalAmount) {

        return new ExtraCheesePromotion(ingredientsRepository, cheeseid, discountAmount, totalAmount);
    }

    @Bean
    MenuService menuService(final RecipesRepository recipesRepository,
                            final List<Promotion> promotions) {

        return new MenuService(recipesRepository, promotions);
    }
}
