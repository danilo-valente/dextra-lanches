package com.dextra.ps.lanches.domain.service;

import com.dextra.ps.lanches.domain.entity.Recipe;
import com.dextra.ps.lanches.domain.service.promotion.PromotionTestsConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PromotionTestsConfiguration.class, properties = {
        "application.ingredient.lettuceId=1",
        "application.ingredient.baconId=2",
        "application.ingredient.beefBurgerId=3",
        "application.ingredient.eggId=4",
        "application.ingredient.cheeseId=5",
        "application.promotion.light.discount=0.1",
        "application.promotion.extraIngredient.discountAmount=1",
        "application.promotion.extraIngredient.totalAmount=3"
})
public class MenuServiceTest {

    @Autowired
    @Qualifier("xBaconRecipe")
    private Recipe xBacon;

    @Autowired
    @Qualifier("xBurgerRecipe")
    private Recipe xBurger;

    @Autowired
    @Qualifier("xEggRecipe")
    private Recipe xEgg;

    @Autowired
    @Qualifier("xEggBaconRecipe")
    private Recipe xEggBacon;

    @Autowired
    private MenuService menuService;

    @Test(expected = IllegalArgumentException.class)
    public void calculateSnackPriceFromRecipe_NullRecipeGiven_ShouldThrowException() {

        menuService.calculateSnackPriceFromRecipe(null);
    }

    @Test
    public void calculateSnackPriceFromRecipe_XBaconRecipeGiven_ShouldReturnCorrectPrice() {

        assertEquals(menuService.calculateSnackPriceFromRecipe(xBacon), 2500);
    }

    @Test
    public void calculateSnackPriceFromRecipe_XBurgerRecipeGiven_ShouldReturnCorrectPrice() {

        assertEquals(menuService.calculateSnackPriceFromRecipe(xBurger), 1500);
    }

    @Test
    public void calculateSnackPriceFromRecipe_XEggRecipeGiven_ShouldReturnCorrectPrice() {

        assertEquals(menuService.calculateSnackPriceFromRecipe(xEgg), 1900);
    }

    @Test
    public void calculateSnackPriceFromRecipe_XEggBaconRecipeGiven_ShouldReturnCorrectPrice() {

        assertEquals(menuService.calculateSnackPriceFromRecipe(xEggBacon), 2900);
    }

    @Test
    public void calculateSnackPriceFromRecipe_LettuceAndTripleCheeseRecipeGiven_ShouldReturnCorrectPrice() {

        assertEquals(menuService.calculateSnackPriceFromRecipe(xEggBacon), 2900);
    }
}
