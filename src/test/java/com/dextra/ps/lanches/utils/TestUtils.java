package com.dextra.ps.lanches.utils;

import com.dextra.ps.lanches.domain.entity.Ingredient;
import com.dextra.ps.lanches.domain.entity.Recipe;

import java.util.Collections;
import java.util.List;

public class TestUtils {

    public static Recipe recipeWithSingleItem(final Ingredient ingredient, final int amount) {
        return recipeWithSingleItem(new Recipe.RecipeItem(ingredient, amount));
    }

    public static Recipe recipeWithSingleItem(final Recipe.RecipeItem item) {
        return recipeWithItems(Collections.singletonList(item));
    }

    public static Recipe recipeWithItems(final List<Recipe.RecipeItem> items) {
        return new Recipe(1L, "Testing recipe", items, "");
    }
}
