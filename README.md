# Dextra Challenge

## Run HTTP Server
```
mvn spring-boot:run
```

A website will be available at `http://localhost:8080/`

## Run Tests
```
mvn test
```

## Build Application
```
mvn package
```
